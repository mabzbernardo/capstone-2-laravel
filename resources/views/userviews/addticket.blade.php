@extends('layouts.app')

@section('content')

<h1 class="text-center">Add Ticket</h1>


<div class="col-lg-6 offset-lg-3">
	<form action="/addticket" method="POST">
		@csrf
		<div class="form-group">
			<label for="comment">Comment:</label>
			<textarea type="text" name="comment" class="form-control"></textarea>
		</div>
		<div class="form-group">
			<label for="date">Date:</label>
			<input type="text" name="date" class="form-control">
		</div>
		<div class="form-group">
			<label for="concern_id">Concern:</label>
			<select name="concern_id" class="form-control">
				@foreach($concerns as $concern)
				<option value="{{$concern->id}}">{{$concern->name}}</option>

				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="borrow_id">Borrow Id:</label>
			<input type="text" name="borrow_id" class="form-control">
		</div>
		<div class="form-group">
			<label for="user_id">Client Name:</label>
			<input type="text" name="user_id" class="form-control">
		</div>
		<div class="form-group">
			<label for="status_id">Status:</label>
			<select name="status_id" class="form-control">
				@foreach($statuses as $status)
				<option value="{{$status->id}}">{{$status->name}}</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-primary" type="submit">Add Ticket</button>
	</form>
</div>

@endsection
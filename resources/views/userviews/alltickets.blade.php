@extends('layouts.app')

@section('content')


<h1 class="text-center">All Tickets</h1>
<div class="container">
	<div class="row">
		@foreach($tickets as $ticket)
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped">
				<thead>
					<tr>
						<th></th>
						<th>Comment: {{$ticket->comment}}</th>
						<th>Date: {{$ticket->date}}</th>
						<th>Concern: {{$ticket->concern_id}}</th>
						<th>Client Name:</th>
						<th>Serial Number: {{$ticket->asset}}</th>
						<th>Status:</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		@endforeach
	</div>
</div>


@endsection
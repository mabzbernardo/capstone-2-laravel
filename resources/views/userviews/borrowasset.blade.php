@extends('layouts.app')
@section('content')

<h1 class="text-center">Borrow Asset</h1>

<div class="col-lg-6 offset-lg-3">
	{{-- <form action="/borrowasset/{{$asset->id}}" method="POST" enctype="multipart/form-data"> --}}
		{{-- @csrf
		@method('PATCH')
		 --}}

		 @foreach($assets as $asset)
		 
		<div class="form-group">
			<label for="name">Asset Name: {{$asset->name}}</label>
			{{-- <input type="text" name="name" class="form-control" value="{{$asset->name}}"> --}}
		</div>
		<div class="form-group">
			<label for="productCode">Product Code: {{$asset->productCode}}</label>
			{{-- <input type="text" name="productCode" class="form-control" value="{{$asset->productCode}}"> --}}
		</div>
		<div class="form-group">
			<label for="price">Product Price: {{$asset->quantity}}</label>
			{{-- <input type="text" name="productCode" class="form-control" value="{{$asset->productCode}}"> --}}
		</div>

		{{-- <div class="form-group">
			<label for="imgPath">Image:</label>
			<input type="file" name="imgPath" class="form-control-file">
		</div> --}}
		<div class="form-group">
			<label for="type_id">Type:</label>
			<select name="type_id" class="form-control">
				@foreach($types as $type)
				<option value="{{$type->id}}" {{$type->id == $asset->type_id ? "selected" : ""}}>{{$type->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="quantity">Quantity:</label>
			<input type="number" name="quantity" class="form-control">
		</div>
		<button class="btn btn-primary" type="submit">Proceed</button>
		<h1>total: {{$total}}</h1>
		@endforeach

	</form>
</div>


@endsection
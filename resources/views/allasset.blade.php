@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">ALL ASSET</h1>

@if(Session::has("message"))
	<h4>{{Session::get('message')}}</h4>
@endif

<div class="container">
	<div class="row">
		@foreach($assets as $asset)
			<div class="col-lg-4 my-2">
				<div class="card">
					<img class="card-img-top" src="{{$asset->imgPath}}" height="200px">
					<div class="card-body">
						<h4 class="card-title">Asset Name: {{$asset->name}}</h4>
						<p class="card-text">Product Code: {{$asset->productCode}}</p>
						<p class="card-text">Asset Type: {{$asset->type->name}}</p>
						<p class="card-text">Availability: {{$asset->availability}}</p>
						<form action="/deleteasset/{{$asset->id}}" method="POST">
							@csrf
							@method('DELETE')
							<button class="btn btn-danger" type="submit">DELETE</button>
						</form>
						<a href="/editasset/{{$asset->id}}" class="btn btn-info">Edit</a>
					</div>
					<div class="card-footer">
						<form action="/borrowasset/{{$asset->id}}" method="POST">
							@csrf
							<input type="number" name="quantity" class="form-control" value="1">
							<button class="btn btn-primary" type="submit">Borrow</button>
						</form>
					</div>
				</div>
			</div>
		@endforeach	
	</div>
</div>

@endsection
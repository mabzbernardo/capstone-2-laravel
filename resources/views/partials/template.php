
<html lang="en"> 
<head>
	<meta charset="UTF-8">
	<title>TSAM</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	
  <link rel="stylesheet" href="{{ URL::asset('css/styles.css') }}" />

	<script>
		$(document).ready(function(){
			$(".hamburger").click(function(){
			   $(".wrapper").toggleClass("collapse");
			});
		});
	</script>
</head>

<body>

<div class="wrapper">
  <div class="top_navbar">
    <div class="hamburger">
       <div class="one"></div>
       <div class="two"></div>
       <div class="three"></div>
    </div>
    <div class="top_menu">
      <div class="logo">@yield('navtitle')</div>
      <ul>
        <li><a href="#">
          <i class="fas fa-user"></i>
          </a></li>
      </ul>
    </div>
  </div>   
  
<div class="sidebar">
  <ul>
    <li><a href="admindashboard">
      <span class="icon"><i class="fas fa-home"></i></span>
      <span class="title">Dashboard</span></a></li>

    <li><a href="#">
      <span class="icon"><i class="fas fa-cog"></i></span>
      <span class="title">Management</span>
      </a>
        <ul>
                <li><a href="addassets"> 
                  <span class="title">Add Asset</span></a></li>
        </ul>
    </li>

    <li><a href="#" class="active">
    <span class="icon"><i class="fas fa-book"></i></span>
      <span class="title">Reports</span>
      </a>
            <li><a href="assignsupport"> 
              <span class="title">Assign Support</span></a></li>
            <li><a href="doneticket"> 
              <span class="title">Ticket Processing</span></a></li>
    </li>
  </ul>
</div>
  
<div class="main_container">

  <div class="item">
  <!-- @yield('button') -->
	</div>
	
	<div class="item">
  <!-- @yield('table') -->
	</div>
	
	<div class="item">
  <!-- @yield('map') -->
	</div>
	
  
</div>


	
  
</div>
	
</body>
</html>
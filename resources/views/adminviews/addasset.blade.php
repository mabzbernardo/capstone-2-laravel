@extends('layouts.app')
@section('content')

<h1 class="text-center">Add Asset</h1>

<div class="col-lg-6 offset-lg-3">
	<form action="/addasset" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="name">Asset Name:</label>
			<input type="text" name="name" class="form-control">
		</div>
		<div class="form-group">
			<label for="productCode">Product Code:</label>
			<input type="text" name="productCode" class="form-control">
		</div>
		<div class="form-group">
			<label for="type_id">Type:</label>
			<select name="type_id" class="form-control">
				@foreach($types as $type)
				<option value="{{$type->id}}">{{$type->name}}</option>

				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="availability">Availability:</label>
			<input type="number" name="availability" class="form-control">
		</div>
		<button class="btn btn-primary" type="submit">Add Asset</button>
	</form>
</div>


@endsection
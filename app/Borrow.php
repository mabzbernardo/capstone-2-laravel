<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    public function asset(){
    	return $this->belongsTo("\App\Asset");
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Concern;
use \App\Ticket;
use \App\Status;
use Auth;

class TicketController extends Controller
{
	public function index(){
    	$tickets = Ticket::all();

    	return view('userviews.alltickets', compact('tickets'));
    }

    public function create(){
    	$concerns = Concern::all();
        $statuses = Status::all();

    	return view('userviews.addticket', compact('concerns','statuses'));
    }

     public function store(Request $req){
        dd($req->asset_id);
    	
        $rules = array(
            "comment" => "required",
            "date" => "required",
            "concern_id" => "required",
            "borrow_id" => "required",
            "user_id" => "required",
            "asset_id" => "required"
        );

        $this->validate($req, $rules);

    	$newTicket = new Ticket;
    	$newTicket->comment = $req->comment;
    	$newTicket->date = $req->date;
    	$newTicket->concern_id = $req->concern_id;
        $newTicket->borrow_id = $req->borrow_id;
    	$newTicket->user_id = Auth::user()->id;
    	$newTicket->asset_id = $req->asset_id;
    	$newTicket->status_id = 2;
    	$newTicket->save();

        Session::flash("message", "$newTicket->name has been added");

    	return redirect('/alltickets');

    }


}

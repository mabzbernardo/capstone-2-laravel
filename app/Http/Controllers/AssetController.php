<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Type;
use \App\Asset;
use Auth;
use Session;
class AssetController extends Controller
{
	public function index(){
    	$assets = Asset::all();

    	return view('allasset', compact('assets'));
    }

    public function create(){
    	$types = Type::all();

    	return view('adminviews.addasset', compact('types'));
    }

    public function store(Request $req){
    	
        //validate
        $rules = array(
            "name" => "required",
            "productCode" => "required",
            "type_id" => "required",
            "availability" => "required",
        );

        $this->validate($req, $rules);
    	// dd($req);

    	$newAsset = new Asset;
    	$newAsset->name = $req->name;
    	$newAsset->productCode = $req->productCode;
    	$newAsset->type_id = $req->type_id;
    	$newAsset->availability = $req->availability;
        
    	$newAsset->save();

        Session::flash("message", "$newAsset->name has been added");

    	return redirect('/allasset');

    }

    public function destroy($id){
        $assetToDelete = Asset::find($id);
        $assetToDelete->delete();

        Session::flash("message", "$assetToDelete->name has been deleted");

        return redirect()->back();
    }

    public function edit($id){
        $asset = Asset::find($id);
        $types = Type::all();

        return view ('adminviews.editasset', compact('asset', 'types'));
    }

    public function update($id, Request $req){
        //validate
        $rules = array(
            "name" => "required",
            "productCode" => "required",
            "type_id" => "required",
            "availability" => "required",
            "imgPath" => "required|image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap",
        );
        $this->validate($req, $rules);
        // dd($req);

        $asset = Asset::find($id);
        $asset->name = $req->name;
        $asset->productCode = $req->productCode;
        $asset->type_id = $req->type_id;
        $asset->availability = $req->availability;

        if($req->file('imgPath') !=null){
            $image = $req->file('imgPath');
            $image_name = time(). ".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $asset->imgPath = $destination.$image_name;
        }

        $asset->save();

        Session::flash("message", "$asset->name has been updated");

        return redirect('/allasset');

    }

    public function borrowAsset($id, Request $req){

        //Check if there is an existing session
        //php counterpart if(isset)$_SESSION
        if(Session::has('borrow')){
            $borrow = Session::get('borrow');
        }else{
            $borrow= [];
        }
        //Check if this is the first time we'll add an item to our cart
        if(isset($borrow[$id])){
            $borrow[$id] += $req->quantity;
        }else{
            $borrow[$id] = $req->quantity;
        }
        // dd($borrow);

        Session::put('borrow', $borrow);
        $asset = Asset::find($id);
        //Flash a message
        Session::flash("message", "$req->quantity of $asset->name successfully added to borrowed item list");
        //Find the item

        //Use the name and the quantity in your flash message
        //Ex: 2 of Item Name successfully added to cart

        return redirect()->back();
    }

    public function showBorrow(){

        //we will create a new array containing item name, price, quantity and subtotal
        //initialize an empty array
        $assets = [];
        $total = 0;

        if(Session::has('borrow')){
            $borrow=Session::get('borrow');
            foreach($borrow as $assetId => $quantity){
                $asset = Asset::find($assetId);
                $asset->quantity = $quantity;
                $asset->totalQuantity = $asset-> $quantity;
                $assets[] = $asset;
                $total += $asset->subtotal;
            }
        }

        return view('userviews.borrowasset', compact('assets','total'));
    }


}

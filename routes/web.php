<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//for all

Route::get('/allasset', 'AssetController@index');


//ADMIN
//add asset
Route::get('/addasset', 'AssetController@create');
//save item
Route::post('/addasset', 'AssetController@store');
//delete item
Route::delete('/deleteasset/{id}', 'AssetController@destroy');
//edit
Route::get('/editasset/{id}', 'AssetController@edit');
//save
Route::patch('/editasset/{id}', 'AssetController@update');

//USER

Route::get('/alltickets', 'TicketController@index');

//borrow asset
Route::post('/borrowasset/{id}', 'AssetController@borrowAsset');

Route::get('/borrowasset/{id}', 'AssetController@showBorrow');

//addticket
Route::get('/addticket', 'TicketController@create');

Route::post('/addticket', 'TicketController@store');


Route::get('/borrowasset', function () {
    return view('/userviews.borrowasset');
});

